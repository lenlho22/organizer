// Core
import { render } from 'react-dom';
import { QueryClientProvider } from 'react-query';
import { Provider } from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom';

// Components
import { App } from './app';

// Instruments
import { queryClient } from './lib/react-query';
import { store } from './lib/redux/init/store';
import './theme/styles/index.scss';
import 'react-datepicker/dist/react-datepicker.css';
import 'react-toastify/dist/ReactToastify.css';

render(
    <Provider store = { store }>
        <QueryClientProvider client = { queryClient }>
            <Router>
                <App />
            </Router>
        </QueryClientProvider>
    </Provider>,
    document.getElementById('root'),
);
