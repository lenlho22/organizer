import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { toast } from 'react-toastify';

import { getErrorMessage, getUiMessages } from '../lib/redux/init/rootSelectors';
import { authActions, uiActions } from '../lib/redux/init/rootActions';

export const useToaster = () => {
    const dispatch = useDispatch();
    const errorMessage = useSelector(getErrorMessage);
    const { infoMessage, successMessage } = useSelector(getUiMessages);

    useEffect(() => {
        if (errorMessage) {
            const notify = () => toast.error(errorMessage, {
                position:        'top-right',
                autoClose:       7000,
                hideProgressBar: false,
                closeOnClick:    true,
                pauseOnHover:    true,
                draggable:       true,
                progress:        undefined,
            });
            notify();
            /**
             * необходимо очистить состояние ошибки что бы при повторном возникновении
             * такой же ошибки появилось вспылвающее сообщение
             * */
            dispatch(authActions.resetError());
        }

        if (successMessage) {
            const notify = () => toast.success(successMessage, {
                position:        'top-right',
                autoClose:       7000,
                hideProgressBar: false,
                closeOnClick:    true,
                pauseOnHover:    true,
                draggable:       true,
                progress:        undefined,
            });
            notify();
            dispatch(uiActions.resetSuccessMessage());
        }

        if (infoMessage) {
            const notify = () => toast.info(infoMessage, {
                position:        'top-right',
                autoClose:       7000,
                hideProgressBar: false,
                closeOnClick:    true,
                pauseOnHover:    true,
                draggable:       true,
                progress:        undefined,
            });
            notify();
            dispatch(uiActions.resetInfoMessage());
        }
    }, [errorMessage, infoMessage, successMessage]);
};
