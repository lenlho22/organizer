/* Core */
import { useEffect } from 'react';
import { useMutation, UseMutationResult } from 'react-query';
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';

/* Other */
import { api } from '../api';
import { authActions } from '../lib/redux/init/rootActions';
import { useApiError } from './useApiError';
import { useSetMessage } from './useSetMessage';
import {
    ICredentialsModel, ErrorType, RoutesEnum, IToken, MessagesEnum,
} from '../types';
import { TOKEN_KEY } from '../constants';

export const useLogin = (): UseMutationResult<IToken, ErrorType, ICredentialsModel> => {
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const { handleError } = useApiError();
    const { setMessage } = useSetMessage();

    const mutation = useMutation<IToken, ErrorType, ICredentialsModel>((credentials) => {
        return api.login(credentials);
    },
    {
        onError:   handleError,
        onSuccess: () => {
            setMessage(MessagesEnum.SUCCESS, 'Добро пожаловать!');
        },
    });

    useEffect(() => {
        if (mutation.isSuccess) {
            if (mutation.data) {
                const  { data: token }  = mutation.data;
                dispatch(authActions.setToken(token));
                localStorage.setItem(TOKEN_KEY, token);
                navigate(RoutesEnum.TASK_MANAGER);
            }
        }
    }, [mutation.isSuccess]);


    return mutation;
};
