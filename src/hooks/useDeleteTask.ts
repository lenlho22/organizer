/* Core */
import { useMutation, UseMutationResult } from 'react-query';
import { useSelector } from 'react-redux';

/* Other */
import { api } from '../api';
import { useApiError } from './useApiError';
import { useSetMessage } from './useSetMessage';
import { getToken } from '../lib/redux/auth/selectors';
import { ErrorType, MessagesEnum } from '../types';

export const useDeleteTask = ():
UseMutationResult<undefined, ErrorType, string> => {
    const { handleError } = useApiError();
    const token = useSelector(getToken);
    const { setMessage } = useSetMessage();

    const mutation = useMutation<undefined, ErrorType, string>((id) => {
        return api.deleteTask(id, token);
    },
    {
        onError:   handleError,
        onSuccess: () => {
            setMessage(MessagesEnum.INFO, 'Задача удалена.');
        },
    });

    return mutation;
};
