import { useDispatch } from 'react-redux';

import { authActions } from '../lib/redux/init/rootActions';
import { ErrorType } from '../types';

export const useApiError = () => {
    const dispatch = useDispatch();

    const handleError = (error: ErrorType) => {
        const { message } = error.response.data;

        if (message) {
            dispatch(authActions.setError(message));
        } else {
            dispatch(authActions.setError('Ошибка запроса. Повторите через несколько минут или обратитесь к администратору.'));
        }
    };

    return {
        handleError,
    };
};
