import { useDispatch } from 'react-redux';

import { uiActions } from '../lib/redux/init/rootActions';
import { MessagesEnum } from '../types';

export const useSetMessage = () => {
    const dispatch = useDispatch();

    const setMessage = (type: MessagesEnum.INFO | MessagesEnum.SUCCESS, message: string) => {
        if (type === MessagesEnum.INFO) {
            dispatch(uiActions.setInfoMessage(message));
        }

        if (type === MessagesEnum.SUCCESS) {
            dispatch(uiActions.setSuccessMessage(message));
        }
    };

    return {
        setMessage,
    };
};
