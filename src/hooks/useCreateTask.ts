/* Core */
import { useMutation, UseMutationResult } from 'react-query';
import { useSelector } from 'react-redux';

/* Other */
import { api } from '../api';
import { useApiError } from './useApiError';
import { useSetMessage } from './useSetMessage';
import { getToken } from '../lib/redux/auth/selectors';
import {
    ITaskModel, ITaskFormModel, ErrorType, MessagesEnum,
} from '../types';

export const useCreateTask = (): UseMutationResult<ITaskModel, ErrorType, ITaskFormModel> => {
    const { handleError } = useApiError();
    const token = useSelector(getToken);
    const { setMessage } = useSetMessage();

    const mutation = useMutation<ITaskModel, ErrorType, ITaskFormModel>((newTask) => {
        return api.createTask(newTask, token);
    },
    {
        onError:   handleError,
        onSuccess: () => {
            setMessage(MessagesEnum.INFO, 'Задача добавлена.');
        },
    });

    return mutation;
};
