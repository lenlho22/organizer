/* Core */
import { useQuery, UseQueryResult } from 'react-query';

/* Other */
import { api } from '../api';
import { ITagModel, ErrorType } from '../types';

export const useTags = (): UseQueryResult<ITagModel[], ErrorType> => {
    const query = useQuery<ITagModel[], ErrorType>('tags', api.getTags);

    return query;
};
