/* Core */
import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { useMutation, UseMutationResult } from 'react-query';

/* Other */
import { api } from '../api';
import { authActions } from '../lib/redux/init/rootActions';
import { TOKEN_KEY } from '../constants';
import { useApiError } from './useApiError';
import { useSetMessage } from './useSetMessage';
import {
    ISignupCredentialsModel, IToken, ErrorType, RoutesEnum, MessagesEnum,
} from '../types';


export const useSignUp = (): UseMutationResult<IToken, ErrorType, ISignupCredentialsModel> => {
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const { handleError } = useApiError();
    const { setMessage } = useSetMessage();

    const mutation = useMutation<IToken, ErrorType, ISignupCredentialsModel>((credentials) => {
        return api.signUp(credentials);
    },
    {
        onError:   handleError,
        onSuccess: () => {
            setMessage(MessagesEnum.SUCCESS, 'Добро пожаловать!');
        },
    });

    useEffect(() => {
        if (mutation.isSuccess) {
            if (mutation.data) {
                const { data: token } = mutation.data;
                dispatch(authActions.setToken(token));
                localStorage.setItem(TOKEN_KEY, token);
                navigate(RoutesEnum.TASK_MANAGER);
            }
        }
    }, [mutation.isSuccess]);

    return mutation;
};
