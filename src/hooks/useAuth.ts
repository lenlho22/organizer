import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';

import { TOKEN_KEY } from '../constants';
import { authActions } from '../lib/redux/init/rootActions';
import { getToken } from '../lib/redux/init/rootSelectors';
import { useSetMessage } from './useSetMessage';
import { RoutesEnum, MessagesEnum } from '../types';

interface IUseAuth {
    logout: () => void;
}

export const useAuth = (): IUseAuth => {
    const dispatch = useDispatch();
    const token  = useSelector(getToken);
    const navigate = useNavigate();
    const { setMessage } = useSetMessage();

    useEffect(() => {
        const localToken = localStorage.getItem(TOKEN_KEY);

        if (localToken && !token) {
            dispatch(authActions.setToken(localToken));

            return;
        }

        if (!localToken && !token) {
            navigate(RoutesEnum.LOGIN);
        }
    }, [token]);

    return {
        logout: () => {
            dispatch(authActions.resetToken());
            localStorage.removeItem(TOKEN_KEY);
            navigate(RoutesEnum.LOGIN);
            setMessage(MessagesEnum.INFO, 'Возвращайтесь поскорее ;) Мы будем скучать.');
        },
    };
};
