/* Core */
import { useEffect } from 'react';
import { useQuery, UseQueryResult } from 'react-query';
import { useDispatch, useSelector } from 'react-redux';

/* Other */
import { api } from '../api';
import { tasksActions } from '../lib/redux/init/rootActions';
import { getToken } from '../lib/redux/auth/selectors';
import { ITaskModel } from '../types';

export const useTasks = (): UseQueryResult<ITaskModel[], Error> => {
    const token = useSelector(getToken);
    const query = useQuery<ITaskModel[], Error>('tips', () => api.getTasks(token));
    const dispatch = useDispatch();

    useEffect(() => {
        if (query.isSuccess) {
            const tasks  = query.data;
            if (tasks) {
                dispatch(tasksActions.setTasks(tasks));
            }
        }
    }, [query.isSuccess]);

    return query;
};
