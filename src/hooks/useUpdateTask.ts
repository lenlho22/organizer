/* Core */
import { useMutation, UseMutationResult } from 'react-query';
import { useSelector } from 'react-redux';

/* Other */
import { api } from '../api';
import { useApiError } from './useApiError';
import { useSetMessage } from './useSetMessage';
import { getToken } from '../lib/redux/auth/selectors';
import {
    ITaskModel, ITaskFormModel, ErrorType, MessagesEnum,
} from '../types';

type VariablesType = {
    task: ITaskFormModel,
    id: string,
};

export const useUpdateTask = ():
UseMutationResult<ITaskModel, ErrorType, VariablesType> => {
    const { handleError } = useApiError();
    const token = useSelector(getToken);
    const { setMessage } = useSetMessage();

    const mutation = useMutation<ITaskModel, ErrorType, VariablesType>(({ task, id }) => {
        return api.updateTask(task, id, token);
    },
    {
        onError:   handleError,
        onSuccess: (data) => {
            setMessage(MessagesEnum.INFO, `Задача с id = ${data.id} успешно обновлена.`);
        },
    });

    return mutation;
};
