// Core
import axios from 'axios';
import { TOKEN_KEY } from '../constants';

import {
    ICredentialsModel,
    ISignupCredentialsModel,
    IToken,
    ITagModel,
    ITaskModel,
    ITaskFormModel,
} from '../types';

export const TODO_API_URL = 'https://lab.lectrum.io/rtx/api/v2/todos';

export const api = {
    async login(credentials: ICredentialsModel): Promise<IToken> {
        const { email, password } = credentials;
        const { data } = await axios.get(
            `${TODO_API_URL}/auth/login`,
            {
                headers: {
                    authorization: `Basic ${btoa(`${email}:${password}`)}`,
                },
            },
        );

        return data;
    },
    async signUp(credentials: ISignupCredentialsModel): Promise<IToken> {
        const { confirmPassword, ...body } = credentials;
        const { data } = await axios.post(
            `${TODO_API_URL}/auth/registration`,
            body,
        );

        return data;
    },
    async getTags(): Promise<ITagModel[]> {
        const { data } = await axios.get(
            `${TODO_API_URL}/tags`,
        );

        return data;
    },
    async createTask(newTask: ITaskFormModel, token: string): Promise<ITaskModel> {
        const body = newTask;

        const { data } = await axios.post(
            `${TODO_API_URL}/tasks`,
            body,
            {
                headers: {
                    authorization: `Bearer ${token}`,
                },
            },
        );

        return data.data;
    },
    async updateTask(newTask: ITaskFormModel, id: string, token: string): Promise<ITaskModel> {
        const body = newTask;

        const { data } = await axios.put(
            `${TODO_API_URL}/tasks/${id}`,
            body,
            {
                headers: {
                    authorization: `Bearer ${token}`,
                },
            },
        );

        return data.data;
    },
    async deleteTask(id: string, token: string): Promise<undefined> {
        await axios.delete(
            `${TODO_API_URL}/tasks/${id}`,
            {
                headers: {
                    authorization: `Bearer ${token}`,
                },
            },
        );

        return undefined;
    },
    async getTasks(token: string): Promise<ITaskModel[]> {
        // Fallback if reload page
        const localToken = localStorage.getItem(TOKEN_KEY);

        const { data } = await axios.get(
            `${TODO_API_URL}/tasks`,
            {
                headers: {
                    authorization: `Bearer ${token || localToken}`, // If reload page
                },
            },
        );

        return data.data;
    },
};
