import { FC } from 'react';
import { Link } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';

import { loginSchema } from './config';
import { useLogin } from '../../../hooks';
import { ICredentialsModel, RoutesEnum } from '../../../types';

import { Input } from '../elements';

export const LoginForm: FC = () => {
    const auth = useLogin();

    const {
        handleSubmit, reset, register, formState,
    } = useForm({
        mode:     'onTouched',
        resolver: yupResolver(loginSchema),
    });

    const login = handleSubmit(async (credentials: ICredentialsModel) => {
        await auth.mutateAsync(credentials);
        reset();
    });

    return (
        <section className = 'sign-form'>
            <form onSubmit = { login }>
                <fieldset disabled = { false }>
                    <legend>Вход</legend>
                    <Input
                        placeholder = 'Электропочта'
                        type = 'email'
                        labelClassName = 'label'
                        error = { formState.errors.email }
                        register = { register('email') } />
                    <Input
                        placeholder = 'Пароль'
                        type = 'password'
                        error = { formState.errors.password }
                        register = { register('password') } />

                    <input
                        className = 'button-login'
                        type = 'submit'
                        value = 'Войти' />
                </fieldset>
                <p>
                    Если у вас до сих пор нет учётной записи, вы можете{ ' ' }
                    <Link to = { RoutesEnum.SIGNUP } >зарегистрироваться</Link>.
                </p>
            </form>
        </section>
    );
};
