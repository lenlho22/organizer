import * as yup from 'yup';
import {
    tooShortMessage, tooLongMessage, wrongEmailMessage, requiredEmail,
} from '../ constants';

export interface ILoginFormShape {
    email: string;
    password: string;
}

export const loginSchema: yup.SchemaOf<ILoginFormShape> = yup.object().shape({
    email: yup
        .string()
        .email(wrongEmailMessage)
        .required(requiredEmail),
    password: yup
        .string()
        .min(8, tooShortMessage)
        .max(64, tooLongMessage)
        .required('*'),
});
