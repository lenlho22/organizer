/* eslint-disable no-template-curly-in-string */
export const tooShortMessage = 'Минимальная длина — ${min} символов';
export const tooLongMessage = 'Максимальная длина — ${max} символов';
export const wrongEmailMessage = 'Почта должна быть настоящей';
export const requiredEmail = 'Поле email обязательно для заполнения';
