/* Core */
import { FC } from 'react';
import { UseFormRegisterReturn } from 'react-hook-form';

interface IPropTypes {
    placeholder?: string;
    type?: string;
    tag?: string;
    label?: string;
    className?: string;
    labelClassName?: string;
    register: UseFormRegisterReturn;
    error?: {
        message?: string;
    };
    options?: { value: string; name: string }[];
}

export const Input: FC<IPropTypes> = (props) => {
    let input = <input
        placeholder = { props.placeholder }
        type = { props.type }
        className = { props.className }
        { ...props.register } />;

    if (props.tag === 'textarea') {
        input = <textarea
            placeholder = { props.placeholder }
            className = { props.className }
            { ...props.register }></textarea>;
    }

    if (props.tag === 'select') {
        const optionsJSX = props.options?.map((option) => {
            return (
                <option key = { option.value } value = { option.value }>
                    { option.name }
                </option>
            );
        });

        input = <select
            className = { props.className }
            { ...props.register }>
            { optionsJSX }
        </select>;
    }

    return (
        <label className = { props.labelClassName }>
            { props.label ? (
                <div>
                    { props.label } <span className = 'errorMessage'>{ props.error?.message }</span>
                </div>
            ) : (
                <span className = 'errorMessage'>{ props.error?.message }</span>
            ) }
            { input }
        </label>
    );
};

Input.defaultProps = {
    type: 'text',
    tag:  'input',
};
