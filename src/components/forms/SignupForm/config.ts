import * as yup from 'yup';
import {
    tooShortMessage, tooLongMessage, wrongEmailMessage, requiredEmail,
} from '../ constants';

interface ISignUpFormShape {
    name: string;
    email: string;
    password: string;
    confirmPassword: string;
}

export const signUpSchema: yup.SchemaOf<ISignUpFormShape> = yup.object().shape({
    name: yup
        .string()
        .min(2, tooShortMessage)
        .max(50, tooLongMessage)
        .required('*'),
    email: yup
        .string()
        .email(wrongEmailMessage)
        .required(requiredEmail),
    password: yup
        .string()
        .min(8, tooShortMessage)
        .max(64, tooLongMessage)
        .required('*'),
    confirmPassword: yup
        .string()
        .oneOf([yup.ref('password')], 'Пароли должны совпадать')
        .required('Поле confirmPassword обязательно для заполнения'),
});
