import { FC } from 'react';
import { useForm } from 'react-hook-form';
import { Link } from 'react-router-dom';
import { yupResolver } from '@hookform/resolvers/yup';

import { signUpSchema } from './config';
import { useSignUp } from '../../../hooks';
import { RoutesEnum, ISignupCredentialsModel } from '../../../types';

import { Input } from '../elements';

export const SignupForm: FC = () => {
    const auth = useSignUp();

    const {
        handleSubmit, register, reset, formState,
    } = useForm({
        mode:     'onTouched',
        resolver: yupResolver(signUpSchema),
    });

    const signup = handleSubmit(async (credentials: ISignupCredentialsModel) => {
        await auth.mutateAsync(credentials);
        reset();
    });

    return (
        <section className = 'sign-form'>
            <form onSubmit = { signup }>
                <fieldset disabled = { false }>
                    <legend>Регистрация</legend>
                    <Input
                        placeholder = 'Имя и фамилия'
                        labelClassName = 'label'
                        error = { formState.errors.name }
                        register = { register('name') } />
                    <Input
                        placeholder = 'Электропочта'
                        labelClassName = 'label'
                        type = 'email'
                        error = { formState.errors.email }
                        register = { register('email') } />
                    <Input
                        placeholder = 'Пароль'
                        labelClassName = 'label'
                        type = 'password'
                        error = { formState.errors.password }
                        register = { register('password') } />
                    <Input
                        placeholder = 'Подтверждение пароля'
                        labelClassName = 'label'
                        type = 'password'
                        error = { formState.errors.confirmPassword }
                        register = { register('confirmPassword') } />

                    <input
                        className = 'button-login' type = 'submit'
                        value = 'Зарегистрироваться' />
                </fieldset>
                <p>
                    Перейти к <Link to = { RoutesEnum.LOGIN }>логину</Link>.
                </p>
            </form>
        </section>
    );
};
