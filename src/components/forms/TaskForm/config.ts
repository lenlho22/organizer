/* eslint-disable no-template-curly-in-string */
import * as yup from 'yup';

interface ITaskFormShape {
    // completed: boolean;
    title: string;
    deadline: string;
    description: string;
    tag: string;
}

export const taskSchema: yup.SchemaOf<ITaskFormShape> = yup.object().shape({
    title: yup
        .string()
        .min(3, 'Минимальная длина поля title — ${min}')
        .max(50, 'Максимальная длина поля title — ${max} символов')
        .required('*'),
    deadline: yup
        .string()
        .required('Поле deadline обязательно для заполнения'),
    description: yup
        .string()
        .min(3, 'Минимальная длина поля description — ${min}')
        .max(150, 'Максимальная длина поля description — ${max} символов')
        .required('*'),
    tag: yup
        .string()
        .required('Поле tag обязательно для заполнения'),
});
