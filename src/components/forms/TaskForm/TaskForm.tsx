import { FC, useState, useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Controller, useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import ReactDatePicker, { registerLocale } from 'react-datepicker';
import ru from 'date-fns/locale/ru';

import { taskSchema } from './config';
import { ITaskFormModel, ITaskModel } from '../../../types';
import { useCreateTask, useDeleteTask, useUpdateTask } from '../../../hooks';
import { tasksActions } from '../../../lib/redux/init/rootActions';
import { getSelectedTask } from '../../../lib/redux/init/rootSelectors';

import { Input } from '../elements';
import { TagsList } from '../../Tags';

registerLocale('ru', ru);

const defaultValues = {
    completed:   false,
    title:       '',
    deadline:    new Date(),
    description: '',
    tag:         '',
};

type Prop = {
    callback?: () => void;
};

const getValuesFromTask = (task: ITaskModel) => {
    const {
        completed, title, deadline, description, tag: { id: tagId },
    } = task;

    return {
        completed, title, deadline: new Date(deadline), description, tag: tagId,
    };
};

export const TaskForm: FC<Prop> = ({ callback }) => {
    const dispatch = useDispatch();
    const createTask = useCreateTask();
    const updateTask = useUpdateTask();
    const deleteTask = useDeleteTask();
    const selectedTask = useSelector(getSelectedTask);
    const [selectedTag, setSelectedTag]
        = useState<string>(selectedTask?.tag.id || defaultValues.tag);

    const {
        handleSubmit, register, reset, formState, control,  setValue,
    } = useForm({
        mode:          'onTouched',
        defaultValues: selectedTask ? getValuesFromTask(selectedTask) : defaultValues,
        resolver:      yupResolver(taskSchema),
    });

    const closeForm = useCallback(() => {
        dispatch(tasksActions.selectTask(null));
        if (callback) {
            callback();
        }
    }, [callback]);

    const onSubmit = async (task: ITaskFormModel) => {
        if (selectedTask && selectedTask.id) {
            const updatedTask  = await updateTask.mutateAsync({ task, id: selectedTask.id });
            if (updatedTask) {
                dispatch(tasksActions.editTask(updatedTask));
            }
        } else {
            const newTask  = await createTask.mutateAsync(task);
            if (newTask) {
                dispatch(tasksActions.addTask(newTask));
            }
        }
        reset();
        closeForm();
    };

    const errorsJSX = () => {
        const errors = Object.entries(formState.errors);

        if (errors.length === 0) {
            return null;
        }

        return  errors.map(([key, value]) => (
            <p key = { key } className = 'errorMessage'>{ value?.message }</p>
        ));
    };

    const onTagClick = (tagId: string) => {
        setValue('tag', tagId, { shouldDirty: true });
        setSelectedTag(tagId);
    };

    const handleReset = () => {
        reset();
        setSelectedTag(selectedTask?.tag.id || defaultValues.tag);
    };

    const handleCompleted = () => {
        setValue('completed', true);
        handleSubmit(onSubmit);
    };

    const handleRemoveTask = async () => {
        const deleteId = selectedTask?.id;
        if (deleteId) {
            await deleteTask.mutateAsync(deleteId);
            dispatch(tasksActions.deleteTask(deleteId));
            closeForm();
        }
    };

    return (
        <form onSubmit = { handleSubmit(onSubmit) }>
            <div className = 'head'>
                <input
                    type = 'checkbox' { ...register('completed') }
                    style = { { display: 'none' } } />
                <button
                    className = { `button-complete-task ${selectedTask?.completed ? 'completed' : ''}` }
                    onClick = { handleCompleted }
                    disabled = { !selectedTask || selectedTask.completed }>
                            завершить
                </button>
                { selectedTask && (
                    <button
                        type = 'button'
                        className = 'button-remove-task'
                        onClick = { handleRemoveTask } />
                ) }
            </div>
            <div className = 'content'>
                <Input
                    placeholder = 'Пройти интенсив по React + Redux + TS + Mobx'
                    label = 'Задачи'
                    labelClassName = 'label'
                    className = 'title'
                    register = { register('title') } />

                <div className = 'deadline'>
                    <span className = 'label'>Дедлайн</span>
                    <span className = 'date'>
                        <Controller
                            control = { control }
                            name = 'deadline'
                            render = { ({
                                field: {
                                    onChange, onBlur, value,
                                },
                            }) => (
                                <ReactDatePicker
                                    dateFormat = 'd MMM yyyy'
                                    locale = 'ru'
                                    minDate = { new Date() }
                                    onChange = { onChange }
                                    onBlur = { onBlur }
                                    selected = { value } />
                            ) } />
                    </span>
                </div>

                <div className = 'description'>
                    <Input
                        placeholder = 'После изучения всех технологий, завершить работу над проектами и найти работу.'
                        label = 'Описание'
                        labelClassName = 'label'
                        className = 'text'
                        tag = 'textarea'
                        register = { register('description') } />
                </div>
                <div className = 'tags'>
                    <input { ...register('tag') } style = { { display: 'none' } } />
                    <TagsList onTagClick = { onTagClick } selectedTag = { selectedTag } />
                </div>
                <div className = 'errors'>
                    { errorsJSX() }
                </div>
                <div className = 'form-controls'>
                    <button
                        type = 'reset'
                        className = 'button-reset-task'
                        disabled = { !formState.isDirty }
                        onClick = { handleReset }>
                                Reset
                    </button>
                    <button
                        type = 'submit'
                        className = 'button-save-task'
                        disabled = { !formState.isDirty }>
                                Save
                    </button>
                    <button
                        type = 'button'
                        className = 'button-reset-task'
                        onClick = { closeForm }
                        style = { { marginLeft: 'auto' } }>
                                Cancel
                    </button>
                </div>
            </div>
        </form>
    );
};
