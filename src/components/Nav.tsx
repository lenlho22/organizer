import { FC } from 'react';
import { useSelector } from 'react-redux';
import { NavLink } from 'react-router-dom';

import { RoutesEnum } from '../types';
import { useAuth } from '../hooks';
import { getToken } from '../lib/redux/init/rootSelectors';

export const Nav: FC = () => {
    const token  = useSelector(getToken);
    const { logout } = useAuth();

    return (
        <nav>
            { !token && (<NavLink
                activeClassName = 'active'
                to = { RoutesEnum.LOGIN }
                aria-current = 'page'>
                Войти
            </NavLink>) }
            <NavLink
                activeClassName = 'active'
                aria-disabled = { !token }
                to = { RoutesEnum.TASK_MANAGER }>
                К задачам
            </NavLink>
            <NavLink
                activeClassName = 'active'
                aria-disabled = { !token }
                to = { RoutesEnum.PROFILE } >
                Профиль
            </NavLink>
            { !!token && (
                <button
                    className = 'button-logout'
                    onClick = { logout }>
                Выйти
                </button>
            ) }
        </nav>
    );
};
