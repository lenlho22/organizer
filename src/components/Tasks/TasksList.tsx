import { FC } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { useTasks } from '../../hooks';
import { getTasks, getSelectedTask } from '../../lib/redux/init/rootSelectors';
import { tasksActions } from '../../lib/redux/init/rootActions';
import { ITaskModel } from '../../types';

import { Task } from './Task';

interface ITaskListProp {
    onClick?: () => void;
}

export const TasksList: FC<ITaskListProp> = ({ onClick }) => {
    const dispatch = useDispatch();
    const { isLoading, isFetched } = useTasks();
    const tasks  = useSelector(getTasks);
    const selectedTask = useSelector(getSelectedTask);

    const isEmtpyList = isFetched &&  tasks.length === 0;

    const onTaskClick = (task: ITaskModel) => {
        if (onClick) {
            onClick();
        }

        if (task.id !== selectedTask?.id) {
            dispatch(tasksActions.selectTask(task));
        }
    };

    const tasksJSX = tasks.map((task) => (
        <Task
            key = { task.id }
            onClick = { onTaskClick }
            { ...task }  />));

    return (
        <div className =   { `list ${isEmtpyList ? 'empty' : ''}` }>
            <div className = 'tasks'>
                { isLoading && !isFetched ? 'Loading...' : tasksJSX }
            </div>
        </div>
    );
};
