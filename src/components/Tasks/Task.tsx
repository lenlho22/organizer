import { FC } from 'react';

import { getDate } from '../../helpers';
import { ITaskModel } from '../../types';

import { Tag } from '../Tags';

interface ITaskProp extends ITaskModel {
    onClick: (id: ITaskModel) => void;
}

export const Task: FC<ITaskProp> = ({  onClick,  children, ...task }) => {
    const {
        completed, title, deadline,  tag,
    } = task;

    return (
        <div className = { `task ${completed ? 'completed' : ''}` } onClick = { () => onClick(task) }>
            <span className = 'title'>{ title }</span>
            <div className = 'meta'>
                <span className = 'deadline'>{ getDate(deadline) }</span>
                <Tag { ...tag } />
            </div>
        </div>
    );
};
