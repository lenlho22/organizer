import { FC } from 'react';
import { ITagModel } from '../../types';

interface ITagProps extends ITagModel {
    onClick?: (id: string) => void;
    isActive?: boolean;
}

export const Tag: FC<ITagProps> = ({ onClick, isActive = true, ...tag }) => {
    const {
        id, name, color, bg,
    } = tag;

    const handleClick = () => {
        if (onClick) {
            onClick(id);
        }
    };

    return (
        <span
            className = { `tag ${isActive ? 'selected' : ''}` }
            onClick = { handleClick }
            style = { { color, backgroundColor: bg } }>
            { name }
        </span>
    );
};
