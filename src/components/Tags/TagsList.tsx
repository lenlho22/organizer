import { FC } from 'react';

import { useTags } from '../../hooks';

import { Tag } from './Tag';

type Props = {
    selectedTag?: string;
    onTagClick: (tagId: string) => void;
};

export const TagsList: FC<Props> = ({ selectedTag = '', onTagClick }) => {
    const { data: tags } = useTags();

    if (!tags || tags.length === 0) {
        return null;
    }

    return (
        <>
            {
                tags.map((tag) => <Tag
                    key = { tag.id }
                    isActive = { selectedTag === tag.id }
                    onClick = { onTagClick }
                    { ...tag } />)
            }
        </>
    );
};
