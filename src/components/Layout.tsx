import { FC } from 'react';

import { Nav } from './Nav';

export const Layout: FC = ({ children }) => {
    return (
        <>
            <Nav />
            <main>
                { children }
            </main>
            <footer>
                <span>
                    &copy; 2021 Lectrum LLC / Елена Павлова / - Все права защищены.
                </span>
            </footer>
        </>
    );
};
