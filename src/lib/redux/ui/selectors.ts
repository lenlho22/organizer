import { IRootState } from '../init/rootReducer';

export const getUiMessages = (state: IRootState) => {
    return state.ui;
};
