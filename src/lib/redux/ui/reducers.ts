import { AnyAction } from 'redux';
import { types } from './types';

export interface IUiState  {
    successMessage: string;
    infoMessage: string;
}

const initialState: IUiState  = {
    successMessage: '',
    infoMessage:    '',
};

export const uiReducer = (state = initialState, action: AnyAction) => {
    const { type, payload } = action;

    switch (type) {
        case types.SET_SUCCESS_MESSAGE: {
            return {
                ...state,
                successMessage: payload,
            };
        }
        case types.RESET_SUCCESS_MESSAGE: {
            return {
                ...state,
                successMessage: '',
            };
        }
        case types.SET_INFO_MESSAGE: {
            return {
                ...state,
                infoMessage: payload,
            };
        }
        case types.RESET_INFO_MESSAGE: {
            return {
                ...state,
                infoMessage: '',
            };
        }
        default: {
            return state;
        }
    }
};
