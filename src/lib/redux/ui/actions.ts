import { types } from './types';

export const uiActions = Object.freeze({
    setSuccessMessage: (message: string) => {
        return {
            type:    types.SET_SUCCESS_MESSAGE,
            payload: message,
        };
    },
    resetSuccessMessage: () => {
        return {
            type: types.RESET_SUCCESS_MESSAGE,
        };
    },
    setInfoMessage: (message: string) => {
        return {
            type:    types.SET_INFO_MESSAGE,
            payload: message,
        };
    },
    resetInfoMessage: () => {
        return {
            type: types.RESET_INFO_MESSAGE,
        };
    },
});
