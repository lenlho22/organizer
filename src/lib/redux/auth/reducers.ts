import { AnyAction } from 'redux';
import { types } from './types';

export interface IAuthState  {
    token: string;
    errorMessage: string;
    error:        boolean;
}

const initialState: IAuthState  = {
    token:        '',
    errorMessage: '',
    error:        false,
};

export const authReducer = (state = initialState, action: AnyAction) => {
    const { type, payload } = action;

    switch (type) {
        case types.SET_TOKEN: {
            return {
                ...state,
                token:        payload,
                error:        false, // очищаем старый статус об ощибке
                errorMessage: '', // очищаем старое сообщение об ошибке
            };
        }
        case types.RESET_TOKEN: {
            return {
                ...state,
                token:        '',
                error:        false, // очищаем старый статус об ощибке
                errorMessage: '', // очищаем старое сообщение об ошибке
            };
        }
        case types.SET_ERROR: {
            return {
                ...state,
                errorMessage: payload,
                error:        true,
            };
        }
        case types.RESET_ERROR: {
            return {
                ...state,
                errorMessage: '',
                error:        false,
            };
        }
        default: {
            return state;
        }
    }
};
