import { IRootState } from '../init/rootReducer';

export const getToken = (state: IRootState) => {
    return state.auth.token;
};

export const getErrorMessage = (state: IRootState) => {
    return state.auth.errorMessage;
};
