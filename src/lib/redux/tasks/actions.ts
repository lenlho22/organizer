import { ITaskModel } from '../../../types/TaskModel';
import { types } from './types';

export const tasksActions = Object.freeze({
    setTasks: (tasks: ITaskModel[]) => {
        return {
            type:    types.SET_TASKS,
            payload: tasks,
        };
    },
    addTask: (newTask: ITaskModel) => {
        return {
            type:    types.ADD_TASK,
            payload: newTask,
        };
    },
    deleteTask: (id: string) => {
        return {
            type:    types.DELETE_TASK,
            payload: id,
        };
    },
    editTask: (updatedTask: ITaskModel) => {
        return {
            type:    types.EDIT_TASK,
            payload: updatedTask,
        };
    },
    selectTask: (task: ITaskModel | null) => {
        return {
            type:    types.SELECT_TASK,
            payload: task,
        };
    },
});
