import { IRootState } from '../init/rootReducer';
import { ITaskModel } from '../../../types/TaskModel';

export const getTasks = (state: IRootState): ITaskModel[] => {
    return state.tasks.tasks;
};

export const getSelectedTask = (state: IRootState): ITaskModel | null => {
    return state.tasks.selectedTask;
};
