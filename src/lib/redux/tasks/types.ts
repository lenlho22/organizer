export const types = Object.freeze({
    SET_TASKS:   'SET_TASKS',
    ADD_TASK:    'ADD_TASK',
    DELETE_TASK: 'DELETE_TASK',
    EDIT_TASK:   'EDIT_TASK',
    SELECT_TASK: 'SELECT_TASK',
});
