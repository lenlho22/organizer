import { AnyAction } from 'redux';

import { ITaskModel } from '../../../types/TaskModel';
import { types } from './types';

export interface ITasksState  {
    tasks: ITaskModel[];
    selectedTask: ITaskModel | null;
}

const initialState: ITasksState  = {
    tasks:        [],
    selectedTask: null,
};

export const tasksReducer = (state = initialState, action: AnyAction) => {
    const { type, payload } = action;

    switch (type) {
        case types.SET_TASKS: {
            return {
                ...state,
                tasks: payload,
            };
        }
        case types.ADD_TASK: {
            return {
                ...state,
                tasks: [payload, ...state.tasks],
            };
        }
        case types.DELETE_TASK: {
            return {
                ...state,
                tasks: state.tasks.filter((task: ITaskModel) => task.id !== payload),
            };
        }
        case types.EDIT_TASK: {
            const { id } = payload;

            return {
                ...state,
                tasks: state.tasks.map((task: ITaskModel) => {
                    if (task.id === id) {
                        return payload;
                    }

                    return task;
                }),
            };
        }
        case types.SELECT_TASK: {
            return {
                ...state,
                selectedTask: payload,
            };
        }
        default: {
            return state;
        }
    }
};
