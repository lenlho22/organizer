// export * from '../auth/actions';
import { authActions } from '../auth/actions';
import { tasksActions } from '../tasks/actions';
import { uiActions } from '../ui/actions';

export {
    authActions,
    tasksActions,
    uiActions,
};
