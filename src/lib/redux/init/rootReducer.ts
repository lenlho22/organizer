// Core
import { combineReducers } from 'redux';

// Reducers
import { authReducer as auth, IAuthState } from '../auth/reducers';
import { tasksReducer as tasks, ITasksState } from '../tasks/reducers';
import { uiReducer as ui, IUiState } from '../ui/reducers';

export interface IRootState {
    auth: IAuthState,
    tasks: ITasksState,
    ui: IUiState,
}

export const rootReducer = combineReducers({
    auth,
    tasks,
    ui,
});
