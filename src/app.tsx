// Core
import { FC } from 'react';
import { Routes, Route, Navigate } from 'react-router-dom';
import { ToastContainer, Slide } from 'react-toastify';

// Instruments
import { RoutesEnum } from './types';
import { useToaster } from './hooks';

// Components
import {
    HomePage,
    LoginPage,
    SignupPage,
    TaskManagerPage,
    ProfilePage,
} from './pages';

export const App: FC = () => {
    useToaster();

    return (
        <>
            <ToastContainer newestOnTop transition = { Slide } />

            <Routes>
                <Route path = { RoutesEnum.HOME } element = { <HomePage /> } />
                <Route path = { RoutesEnum.LOGIN } element = { <LoginPage /> } />
                <Route path = { RoutesEnum.SIGNUP } element = { <SignupPage /> } />
                <Route path = { RoutesEnum.TASK_MANAGER } element = { <TaskManagerPage /> } />
                <Route path = { RoutesEnum.PROFILE } element = { <ProfilePage /> } />

                <Route path = '*' element = { <Navigate to = { RoutesEnum.HOME } /> } />
            </Routes>
        </>
    );
};

