import { FC, useEffect } from 'react';
import { useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';

import { getToken } from '../lib/redux/init/rootSelectors';
import { RoutesEnum } from '../types';

import { Layout } from '../components';

export const HomePage:FC = () => {
    const token  = useSelector(getToken);
    const navigate = useNavigate();

    useEffect(() => {
        if (token) {
            navigate(RoutesEnum.TASK_MANAGER);
        }
    }, [token]);

    return (
        <Layout>
            <h1>
                HomePage
            </h1>
        </Layout>
    );
};
