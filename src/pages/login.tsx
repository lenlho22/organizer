import { FC } from 'react';

import { Layout } from '../components';
import { LoginForm } from '../components/forms';

export const LoginPage:FC = () => {
    return (
        <Layout>
            <LoginForm />
        </Layout>
    );
};
