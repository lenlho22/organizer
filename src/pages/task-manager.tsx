import { FC, useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { getSelectedTask } from '../lib/redux/init/rootSelectors';
import { tasksActions } from '../lib/redux/init/rootActions';

import { Layout } from '../components';
import { TasksList } from '../components/Tasks';
import { TaskForm } from '../components/forms';

export const TaskManagerPage:FC = () => {
    const dispatch = useDispatch();
    const [showFormCard, setShowFormCard] = useState<boolean>(false);
    const [showForm, setShowForm] = useState<boolean>(false);
    const [shouldShowForm, setShouldShowForm] = useState<boolean>(false);
    const selectedTask = useSelector(getSelectedTask);

    const updateTaskForm = () => {
        setShowForm(false);
        setTimeout(() => {
            setShowForm(true);
        }, 0);
    };

    useEffect(() => {
        if (selectedTask || shouldShowForm) {
            setShowFormCard(true);
            updateTaskForm();
        } else {
            setShowFormCard(false);
        }
    }, [selectedTask, shouldShowForm]);


    const handleShowEmptyForm = () => {
        dispatch(tasksActions.selectTask(null));

        setShowFormCard(true);
        setShouldShowForm(true);
    };

    const handleHideForm = () => {
        setShouldShowForm(false);
    };

    const formJSX = showForm && shouldShowForm
        ? <TaskForm callback = { handleHideForm } />
        : null;

    return (
        <Layout>
            <div className = 'controls'>
                <i className = 'las'></i>
                <button className = 'button-create-task' onClick = { handleShowEmptyForm }>Новая задача</button>
            </div>
            <div className = 'wrap'>
                <TasksList onClick = { () => setShouldShowForm(true) } />
                { showFormCard && (
                    <div className = 'task-card'>
                        { formJSX }
                    </div>
                ) }
            </div>
        </Layout>
    );
};
