import { FC } from 'react';

import { Layout } from '../components';

export const ProfilePage:FC = () => {
    return (
        <Layout>
            <h1>
                Профиль пользователя
            </h1>
        </Layout>
    );
};
