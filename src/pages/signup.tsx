import { FC } from 'react';

import { Layout } from '../components';
import { SignupForm } from '../components/forms';

export const SignupPage:FC = () => {
    return (
        <Layout>
            <SignupForm />
        </Layout>
    );
};
