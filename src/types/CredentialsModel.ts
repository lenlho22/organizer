export interface ICredentialsModel {
    email: string;
    password: string;
}

export interface ISignupCredentialsModel extends ICredentialsModel {
    confirmPassword: string;
}

export interface IToken {
    data: string;
}

export type ErrorType = {
    response: {
        data: {
            statusCode: number,
            message:  string,
            error:    string,
        }
    }
};
