import { ITagModel } from './TagModel';

export interface ITaskFormModel {
    completed: boolean;
    title: string;
    deadline: string;
    description: string;
    tag: string;
}

export interface ITaskModel {
    id: string;
    created: string;
    completed: boolean;
    title: string;
    deadline: string;
    description: string;
    tag: ITagModel
}
