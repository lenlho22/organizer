export enum RoutesEnum  {
    HOME = '/',
    LOGIN = '/login',
    SIGNUP = '/signup',
    TASK_MANAGER = '/task-manager',
    PROFILE = '/profile',
}
